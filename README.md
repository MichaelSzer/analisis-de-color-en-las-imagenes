## Repositorio en Espa�ol

# LEER ME #

Consola de comandos interactivos para detectar colores en las imagenes. 

### Para que sirve este repositorio? ###

Para detectar colores en las imagenes de forma sencilla y rapida.

### Como corro la aplicacion? ###

Tenes que tener instalado Python3 junto con la libreria PIL (Pillow).

Las imagenes que quiera analizar tienen que estar dentro de la carpeta 'Images'.

Para correr el programa tiene que correr el script *Application.py* con el comando `python`, por ejemplo:

	- python3 Application.py

### Programadores ###

- Michael Szerman

### Con quien puedo comunicarme? ###

Por cualquier duda o inconveniente comunicarse con: *michaelszer@gmail.com*