from PIL import Image
import os

class Program():
	'Class to share variables within functions without making them global'
	originalImage = None
	modifiableImage = None
	# Store rotation degree
	imageRotation = 0

	# Store color range to search [ [from], [to] ]
	colorRangesDict = { 'dark-brown': [(0, 0, 0), (120,70,40)], 'yellow': [(80, 60, 0), (210, 180, 210)],
						'sky-blue': [(0, 110, 110), (120, 220, 220)], 'dark-green': [(0, 40, 0), (80, 255, 80)],
						'blue': [(0, 0, 80), (102, 102, 255)] }
	markerColor = (255, 255, 255)

	def FindColor(self, colorRange):

		# Copy original image to draw over a clean one
		Program.modifiableImage = Program.originalImage.copy()
		coordinates = []

		for x in range(Program.modifiableImage.width):
			for y in range(Program.modifiableImage.height):
				[r,g,b] = Program.modifiableImage.getpixel((x,y))
				if colorRange[0][0] <= r <= colorRange[1][0] and colorRange[0][1] <= g <= colorRange[1][1] and colorRange[0][2] <= b <= colorRange[1][2]:
					coordinates.append((x,y))


		print("\n" + str(len(coordinates)) + " pixeles coinciden con el color.\n")

		while(True):
			answer = input("Quiere ver las coordenadas de los pixeles encontradas? [Si/No] ").lower()

			if answer == 'si':
				Program.seeCoordinates = True
				break
			elif answer == 'no':
				Program.seeCoordinates = False
				break
			else:
				print("Error, tiene que ingresar 'Si' o 'No'.")


		# Draw over filtered pixels
		for coor in coordinates:
			if Program.seeCoordinates:
				print(coor, end=' ')
			Program.modifiableImage.putpixel(coor, Program.markerColor)

		print("\n")

		# Rotate image to it original rotation
		Program.modifiableImage = Program.modifiableImage.rotate(Program.imageRotation, expand=True)
		Program.modifiableImage.show()

		self.NextCommand()		


	def FindColorGuide(self):
		print("Podes elejir tu propio rango de colores o usar un predertminado.")
		print("Colores disponibles: ", end='')
		for color in tuple(Program.colorRangesDict.keys()):
			print(color, end=', ')

		print("\n\nEjemplos de colores: 'dark-brown' o '10 24 84 hasta 100 40 60'\n")

		verifyAnswer = False

		while not verifyAnswer:

			answer = input("Color: ")
			words = answer.split()

			# Check if a predertime color was introduced
			if len(words) == 1:
				if words[0] in Program.colorRangesDict:
					verifyAnswer = True
					self.FindColor(Program.colorRangesDict[words[0]])
				else:
					print("Error, el color seleccionado no es valido. Inserte otro color.\n")
			elif 1 < len(words) < 7 or 7 < len(words):
				print("Error, cantidad de argumentos incorrectos. Intentelo de nuevo.\n")
			else:
				# Remove the 'to' from the list
				del words[3]
				numbers = tuple(map(int, words))

				verifyNumbers = True

				# Verify that all remaining input are numbers
				for num in numbers:
					if not type(num) is int:
						print("Error, todos los rangos tienen que ser numeros. Intentelo de nuevo.\n")
						verifyNumbers = False
						break
					elif not 0 <= num <= 255:
						print("Error, todos los numeros tienen que estar entre 0 y 255. Intentelo de nuevo.\n")
						verifyNumbers = False
						break

				if not verifyNumbers:
					continue

				# Transform input into color range
				customColorRange = [(numbers[0], numbers[1], numbers[2]), (numbers[3], numbers[4], numbers[5])]

				if customColorRange[0][0] <= customColorRange[1][0] and customColorRange[0][1] <= customColorRange[1][1] and customColorRange[0][2] <= customColorRange[1][2]:  				
					verifyAnswer = True
					self.FindColor(customColorRange)
				else:
					print("Error con los valores, todos tienen que ser crecientes. r1 <= r2, g1 <= g2, b1 <= b2 .\n")


	def RotateImage(self):
		answer = input("\nCuantos grados quiere rotar la imagen? ")
		try:
			Program.imageRotation = int(answer)
		except (TypeError, ValueError):
			print("Error, tiene que introducir un numero entre 0 a 360")
			self.RotateImage()
			return

		if not 0 <= Program.imageRotation <= 360:
			print("Error, tiene que introducir un numero entre 0 a 360")
			self.RotateImage()
			return

		Program.modifiableImage = Program.originalImage.copy().rotate(Program.imageRotation, expand=True)
		Program.modifiableImage.show()
		self.NextCommand()


	def ChangeMarkerColor(self):
		print("El color del marcador tiene que estar en formato RGB. Ejemplo de entrada: '140, 255, 140' ")
		answer = input("Color: ").lower()
		words = answer.split()

		try:
			Program.markerColor = tuple(map(int, words))
		except (TypeError, ValueError):
			print("Error, el color del marcador tiene que tener tres numeros entre 0 y 255.")
			self.ChangeMarkerColor()
			return

		for color in Program.markerColor:
			if not 0 <= color <= 255:
				print("\nError, el color del marcador tiene que tener tres numeros entre 0 y 255.\n")
				self.ChangeMarkerColor()
				return

		print("\nColor del marcador cambiado exitosamente.")
		self.NextCommand()


	def ImageDimension(self):
		print("\nLa imangen tiene " + str(Program.originalImage.width) + " pixeles de largo y " + str(Program.originalImage.height) + " pixeles de altura.")
		print("Teniendo un total de " + str(Program.originalImage.width*Program.originalImage.height) + " pixeles.\n")
		self.NextCommand()


	def NextCommand(self):
		print("\nSeleccione uno de los siguientes comandos:\n - Encontrar color \n - Rotar imagen \n - Ver dimensiones de la imagen \n - Cambiar color del marcador \n - Cambiar imagen \n - Salir \n \n")
		answer = input().lower()

		if answer == 'encontrar color':
			self.FindColorGuide()
		elif answer == 'rotar imagen':
			self.RotateImage()
		elif answer == 'salir':
			print("\n-------------------------------------------------------\nMuchas gracias por haber usado Encuentra Colores, te esperamos de vuelta.\n-------------------------------------------------------\n")
		elif answer == 'cambiar color del marcador':
			self.ChangeMarkerColor()
		elif answer == 'cambiar imagen':
			self.SetImage()
		elif answer == 'ver dimensiones de la imagen':
			self.ImageDimension()
		else:
			self.NextCommand()


	# Determine image
	def SetImage(self):
		# Reset image rotation
		Program.imageRotation = 0

		imageName = input("\nIntroduzca el nombre de la imagen junto con su extension (.jpeg, .png, etc...): ")

		if not type(imageName) == str:
			print("Error, el nombre tiene que ser una palabra.")
			self.SetImage()
			return

		imagePath = "Images/" + imageName

		# Check if image exist
		if os.path.isfile(imagePath):
			Program.originalImage = Image.open(imagePath).convert('RGB')
			Program.modifiableImage = Program.originalImage.copy()
			self.NextCommand()
		else:
			print("\nLa imagen no ha sido encontrada, asegurese que este en la carpeta 'Images' y que la extension sea la correcta.")
			self.SetImage()


	def Start(self):
		self.SetImage()


Program().Start()